package com.master.domain;

public class MovieQuery {
    private String apikey;
    private String t;

    public MovieQuery() {
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

}
