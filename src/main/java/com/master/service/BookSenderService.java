package com.master.service;

import com.master.domain.Movie;
import com.master.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookSenderService {

    private static final Logger log = LoggerFactory.getLogger(BookSenderService.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

//    @Scheduled(fixedDelay = 3000L)
    public void sendMessage() {
        final Movie message = new Movie("Polar express", "not-Tarantino", true);
        log.info("Sending message to rabbit...");
        rabbitTemplate.convertAndSend(Constants.EXCHANGE_NAME, Constants.ROUTING_KEY, message);
    }
}
