package com.master.service;

import com.master.domain.MovieResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class OmdbClient {

    private static final String uri = "http://www.omdbapi.com/";
    private static final String apiKey = "86164a54";

    private RestTemplate restTemplate;

    @Autowired
    public OmdbClient(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public MovieResponse queryByMovieName(String movieName) {

        HttpHeaders headers = new HttpHeaders();
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uri)
                .queryParam("t", movieName)
                .queryParam("apikey", apiKey);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<MovieResponse> movieResponseEntity = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET,  entity,MovieResponse.class);
        //old version
        //ResponseEntity<MovieResponse> movieResponseEntity = restTemplate.getForEntity("http://www.omdbapi.com/?t=Christmas&apikey=86164a54", MovieResponse.class);
        return movieResponseEntity.getBody();
    }
}
