package com.master.service;

import com.master.domain.Movie;
import com.master.domain.MovieResponse;
import com.master.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class MovieReceiverService {

    private static final Logger log = LoggerFactory.getLogger(MovieReceiverService.class);

    @Autowired
    private OmdbClient omdbClient;

    @RabbitListener(queues = Constants.QUEUE_GENERIC_NAME)
    public void receiveMessage(final Message message) {
        log.info("Received message as generic: {}", message.toString());
    }

    @RabbitListener(queues = Constants.QUEUE_SPECIFIC_NAME)
    public MovieResponse receiveMessage(final Movie movie) {
        log.info("Received message as Movie specific class: {}", movie.toString());
        log.info("Sending request to 3rd party omdbapi");
        MovieResponse movieResponse = omdbClient.queryByMovieName(movie.getName());
        log.info("Received response from 3rd party omdbapi {}", movieResponse);
        return movieResponse;
    }
}
