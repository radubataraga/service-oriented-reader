package com.master.controller;

import com.master.service.BookSenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.master.utils.Constants.SETTINGS_ENDPOINT;

@RestController
public class QueryController {

    @Autowired
    private BookSenderService bookSenderService;

    private Logger logger = LoggerFactory.getLogger(QueryController.class);

    @RequestMapping(value = SETTINGS_ENDPOINT, method = RequestMethod.GET)
    public ResponseEntity<String> retrieveAllPartnersSettings(@RequestParam String clientId) {
        logger.info("GET for targeted movie {}", clientId);
        bookSenderService.sendMessage();
        return ResponseEntity.ok().body("processed body " + clientId);
    }
}
